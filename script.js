/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?

setInterval - працює постійно поки не зупинити
setTimeout - спрацювує один раз, але теє можна зупинити виконання

2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?

Ні. Функції не гарантують точний час виконання, так як на їх можуть впливати завантаження процесора та браузеру 

3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

Використати метод clearInterval, в зякій потрібно передати зміну, яка буде виконувати setTimeout або setInterval

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

const bnt = document.querySelector('.bnt')
const content = document.querySelector('.div-content')
const counter = document.querySelector('.counter')

bnt.addEventListener('click', () => {
  content.innerHTML = ''
  setTimeout(() => {
    content.innerHTML = ''
    let text = document.createElement('p')
    text.textContent = 'Операція виконана успішно'
    content.prepend(text)
  }, 3000)
})

counter.textContent = 10
let counterText = setInterval(() => {
  counter.textContent = counter.textContent - 1
  if (+counter.textContent === 0) {
    counter.textContent = 'Зворотній відлік завершено'
    clearInterval(counterText)
  }
}, 1000)
